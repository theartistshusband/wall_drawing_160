use nannou::prelude::*;

const WIDTH: u32 = 600;
const HEIGHT: u32 = 600;

fn main() {
    nannou::sketch(view).size(WIDTH, HEIGHT).run();
}

fn view(app: &App, frame: Frame) {
    let draw = app.draw();
    draw.background()
        .color(LIGHTGRAY);

    let rect = app.window_rect();

    draw.line()
        .start(vec2(rect.left()+20., rect.top()-20.))
        .end(vec2(rect.right()-20., rect.top()-20.))
        .weight(10.)
        .color(BLACK);
    draw.line()
        .start(vec2(rect.left()+20., rect.bottom()+20.))
        .end(vec2(rect.right()-20., rect.bottom()+20.))
        .weight(10.)
        .color(BLACK);
    draw.line()
        .start(vec2(rect.left()+20., rect.top()-20.))
        .end(vec2(rect.left()+20., rect.bottom()+20.))
        .weight(10.)
        .color(BLACK);
    draw.line()
        .start(vec2(rect.right()-20., rect.top()-20.))
        .end(vec2(rect.right()-20., rect.bottom()+20.))
        .weight(10.)
        .color(BLACK);

    draw.line()
        .start(vec2(rect.left()+40., rect.top()-40.))
        .end(vec2(rect.right()-40., rect.bottom()+40.))
        .weight(10.)
        .color(RED);
    draw.line()
        .start(vec2(rect.right()-40., rect.top()-40.))
        .end(vec2(rect.left()+40., rect.bottom()+40.))
        .weight(10.)
        .color(RED);

    draw.to_frame(app, &frame).unwrap();

    if app.elapsed_frames() == 1 {
        let file_path = captured_frame_path(app, &frame);
        app.main_window().capture_frame(file_path);
    }
}

fn captured_frame_path(app: &App, frame: &Frame) -> std::path::PathBuf {
    app.project_path()
        .expect("failed to locate `project_path`")
        .join("frames")
        .join("wall_drawing_160")
        .with_extension("png")
}
